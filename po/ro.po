# Romanian translations for malcontent package.
# Copyright (C) 2020 THE malcontent'S COPYRIGHT HOLDER
# This file is distributed under the same license as the malcontent package.
# Automatically generated, 2020.
#
# Translators:
# georgian balan <braila222@gmail.com>, 2020
#
msgid ""
msgstr ""
"Project-Id-Version: malcontent\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-10 12:08+0100\n"
"PO-Revision-Date: 2020-04-17 21:58+0000\n"
"Last-Translator: georgian balan <braila222@gmail.com>, 2020\n"
"Language-Team: Romanian (https://www.transifex.com/endless-os/teams/9016/"
"ro/)\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?"
"2:1));\n"

#: accounts-service/com.endlessm.ParentalControls.policy.in:4
msgid "Change your own app filter"
msgstr "Modifica filtrul aplicatiei"

#: accounts-service/com.endlessm.ParentalControls.policy.in:5
msgid "Authentication is required to change your app filter."
msgstr "Pentru a schimba filtrul este necesara autentificarea"

#: accounts-service/com.endlessm.ParentalControls.policy.in:14
msgid "Read your own app filter"
msgstr "Citeste filtrul aplicatiei"

#: accounts-service/com.endlessm.ParentalControls.policy.in:15
msgid "Authentication is required to read your app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:24
msgid "Change another user’s app filter"
msgstr "Schimba filtrul aplicatiei a altui utilizator"

#: accounts-service/com.endlessm.ParentalControls.policy.in:25
msgid "Authentication is required to change another user’s app filter."
msgstr ""
"Pentru a schimba filrtul aplicatiei a altui utilizator,este necesara "
"autentificarea."

#: accounts-service/com.endlessm.ParentalControls.policy.in:34
msgid "Read another user’s app filter"
msgstr "Citeste filtrul aplicatiei a altui utilizator."

#: accounts-service/com.endlessm.ParentalControls.policy.in:35
msgid "Authentication is required to read another user’s app filter."
msgstr ""
"Pentru a citi filtrul aplicatiei a altui utilizator,este necesara "
"autentificarea."

#: accounts-service/com.endlessm.ParentalControls.policy.in:44
msgid "Change your own session limits"
msgstr "Modifica limita de sesiune personala."

#: accounts-service/com.endlessm.ParentalControls.policy.in:45
msgid "Authentication is required to change your session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:54
msgid "Read your own session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:55
msgid "Authentication is required to read your session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:64
msgid "Change another user’s session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:65
msgid "Authentication is required to change another user’s session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:74
msgid "Read another user’s session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:75
msgid "Authentication is required to read another user’s session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:84
msgid "Change your own account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:85
msgid "Authentication is required to change your account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:94
msgid "Read your own account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:95
msgid "Authentication is required to read your account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:104
msgid "Change another user’s account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:105
msgid "Authentication is required to change another user’s account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:114
msgid "Read another user’s account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:115
msgid "Authentication is required to read another user’s account info."
msgstr ""

#: libmalcontent/app-filter.c:694
#, c-format
msgid "App filter for user %u was in an unrecognized format"
msgstr ""

#: libmalcontent/app-filter.c:725
#, c-format
msgid "OARS filter for user %u has an unrecognized kind ‘%s’"
msgstr ""

#: libmalcontent/manager.c:283 libmalcontent/manager.c:412
#, c-format
msgid "Not allowed to query app filter data for user %u"
msgstr ""

#: libmalcontent/manager.c:288
#, c-format
msgid "User %u does not exist"
msgstr ""

#: libmalcontent/manager.c:394
msgid "App filtering is globally disabled"
msgstr ""

#: libmalcontent/manager.c:777
msgid "Session limits are globally disabled"
msgstr ""

#: libmalcontent/manager.c:795
#, c-format
msgid "Not allowed to query session limits data for user %u"
msgstr ""

#: libmalcontent/session-limits.c:306
#, c-format
msgid "Session limit for user %u was in an unrecognized format"
msgstr ""

#: libmalcontent/session-limits.c:328
#, c-format
msgid "Session limit for user %u has an unrecognized type ‘%u’"
msgstr ""

#: libmalcontent/session-limits.c:346
#, c-format
msgid "Session limit for user %u has invalid daily schedule %u–%u"
msgstr ""

#. TRANSLATORS: This is the formatting of English and localized name
#. of the rating e.g. "Adults Only (solo adultos)"
#: libmalcontent-ui/gs-content-rating.c:75
#, c-format
msgid "%s (%s)"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:209
msgid "General"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:218
msgid "ALL"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:222
#: libmalcontent-ui/gs-content-rating.c:485
msgid "Adults Only"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:224
#: libmalcontent-ui/gs-content-rating.c:484
msgid "Mature"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:226
#: libmalcontent-ui/gs-content-rating.c:483
msgid "Teen"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:228
#: libmalcontent-ui/gs-content-rating.c:482
msgid "Everyone 10+"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:230
#: libmalcontent-ui/gs-content-rating.c:481
msgid "Everyone"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:232
#: libmalcontent-ui/gs-content-rating.c:480
msgid "Early Childhood"
msgstr ""

#. Translators: the placeholder is a user’s full name
#: libmalcontent-ui/restrict-applications-dialog.c:222
#, c-format
msgid "Restrict %s from using the following installed applications."
msgstr ""

#: libmalcontent-ui/restrict-applications-dialog.ui:6
#: libmalcontent-ui/restrict-applications-dialog.ui:12
msgid "Restrict Applications"
msgstr ""

#: libmalcontent-ui/restrict-applications-selector.ui:24
msgid "No applications found to restrict."
msgstr ""

#. Translators: this is the full name for an unknown user account.
#: libmalcontent-ui/user-controls.c:242 libmalcontent-ui/user-controls.c:253
msgid "unknown"
msgstr ""

#: libmalcontent-ui/user-controls.c:338 libmalcontent-ui/user-controls.c:425
#: libmalcontent-ui/user-controls.c:711
msgid "All Ages"
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:514
#, c-format
msgid ""
"Prevents %s from running web browsers. Limited web content may still be "
"available in other applications."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:519
#, c-format
msgid "Prevents specified applications from being used by %s."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:524
#, c-format
msgid "Prevents %s from installing applications."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:529
#, c-format
msgid "Applications installed by %s will not appear for other users."
msgstr ""

#: libmalcontent-ui/user-controls.ui:17
msgid "Application Usage Restrictions"
msgstr ""

#: libmalcontent-ui/user-controls.ui:68
msgid "Restrict _Web Browsers"
msgstr ""

#: libmalcontent-ui/user-controls.ui:152
msgid "_Restrict Applications"
msgstr ""

#: libmalcontent-ui/user-controls.ui:231
msgid "Software Installation Restrictions"
msgstr ""

#: libmalcontent-ui/user-controls.ui:281
msgid "Restrict Application _Installation"
msgstr ""

#: libmalcontent-ui/user-controls.ui:366
msgid "Restrict Application Installation for _Others"
msgstr ""

#: libmalcontent-ui/user-controls.ui:451
msgid "Application _Suitability"
msgstr ""

#: libmalcontent-ui/user-controls.ui:473
msgid ""
"Restricts browsing or installation of applications to applications suitable "
"for certain ages or above."
msgstr ""

#. Translators: This is the title of the main window
#. Translators: the name of the application as it appears in a software center
#: malcontent-control/application.c:105 malcontent-control/main.ui:12
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:9
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:3
msgid "Parental Controls"
msgstr ""

#: malcontent-control/application.c:270
msgid "Copyright © 2019, 2020 Endless Mobile, Inc."
msgstr ""

#. Translators: this should be "translated" to the
#. names of people who have translated Malcontent into
#. this language, one per line.
#: malcontent-control/application.c:275
msgid "translator-credits"
msgstr ""

#. Translators: "Malcontent" is the brand name of this
#. project, so should not be translated.
#: malcontent-control/application.c:281
msgid "Malcontent Website"
msgstr ""

#: malcontent-control/application.c:299
msgid "The help contents could not be displayed"
msgstr ""

#: malcontent-control/application.c:336
msgid "Failed to load user data from the system"
msgstr ""

#: malcontent-control/application.c:338
msgid "Please make sure that the AccountsService is installed and enabled."
msgstr ""

#: malcontent-control/carousel.ui:48
msgid "Previous Page"
msgstr ""

#: malcontent-control/carousel.ui:74
msgid "Next Page"
msgstr ""

#: malcontent-control/main.ui:93
msgid "Permission Required"
msgstr ""

#: malcontent-control/main.ui:107
msgid ""
"Permission is required to view and change user parental controls settings."
msgstr ""

#: malcontent-control/main.ui:148
msgid "No Child Users Configured"
msgstr ""

#: malcontent-control/main.ui:162
msgid ""
"No child users are currently set up on the system. Create one before setting "
"up their parental controls."
msgstr ""

#: malcontent-control/main.ui:174
msgid "Create _Child User"
msgstr ""

#: malcontent-control/main.ui:202
msgid "Loading…"
msgstr ""

#: malcontent-control/main.ui:265
msgid "_Help"
msgstr "A_jutor"

#: malcontent-control/main.ui:269
msgid "_About Parental Controls"
msgstr ""

#. Translators: the brief summary of the application as it appears in a software center.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:12
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:4
msgid "Set parental controls and monitor usage by users"
msgstr ""

#. Translators: These are the application description paragraphs in the AppData file.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:16
msgid ""
"Manage users’ parental controls restrictions, controlling how long they can "
"use the computer for, what software they can install, and what installed "
"software they can run."
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:39
msgid "The GNOME Project"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:50
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:59
msgid "Minor improvements to parental controls application UI"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:51
msgid "Add a user manual"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:52
msgid "Translation updates"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:60
msgid "Translations to Ukrainian and Polish"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:67
msgid "Improve parental controls application UI and add icon"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:68
msgid "Support for indicating which accounts are parent accounts"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:75
msgid "Initial release of basic parental controls application"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:76
msgid "Support for setting app installation and run restrictions on users"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:83
msgid "Maintenance release of underlying parental controls library"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:7
msgid "org.freedesktop.MalcontentControl"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localise the semicolons! The list MUST also end with a semicolon!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:13
msgid ""
"parental controls;screen time;app restrictions;web browser restrictions;oars;"
"usage;usage limit;kid;child;"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:9
msgid "Manage parental controls"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:10
msgid "Authentication is required to read and change user parental controls"
msgstr ""

#: malcontent-control/user-selector.c:426
msgid "Your account"
msgstr "Contul tău"

#. Always allow root, to avoid a situation where this PAM module prevents
#. * all users logging in with no way of recovery.
#: pam/pam_malcontent.c:142 pam/pam_malcontent.c:188
#, c-format
msgid "User ‘%s’ has no time limits enabled"
msgstr ""

#: pam/pam_malcontent.c:151 pam/pam_malcontent.c:172
#, c-format
msgid "Error getting session limits for user ‘%s’: %s"
msgstr ""

#: pam/pam_malcontent.c:182
#, c-format
msgid "User ‘%s’ has no time remaining"
msgstr ""

#: pam/pam_malcontent.c:200
#, c-format
msgid "Error setting time limit on login session: %s"
msgstr ""

#~ msgid "No cartoon violence"
#~ msgstr "Fără violență animată"

#~ msgid "Cartoon characters in unsafe situations"
#~ msgstr "Caractere animate în siutații nesigure"

#~ msgid "Cartoon characters in aggressive conflict"
#~ msgstr "Caractere animate în conflict agresiv"

#~ msgid "Graphic violence involving cartoon characters"
#~ msgstr "Violență grafică incluzând caractere animate"

#~ msgid "No fantasy violence"
#~ msgstr "Fără violență fantastică"

#~ msgid "Characters in unsafe situations easily distinguishable from reality"
#~ msgstr "Caractere în situații nesigure recunoscute ușor din realitate"

#~ msgid ""
#~ "Characters in aggressive conflict easily distinguishable from reality"
#~ msgstr "Caractere în conflict agresiv recunoscute ușor din realitate"

#~ msgid "Graphic violence easily distinguishable from reality"
#~ msgstr "Violență grafică recunoscută ușor din realitate"

#~ msgid "No realistic violence"
#~ msgstr "Fără violență reală"

#~ msgid "Mildly realistic characters in unsafe situations"
#~ msgstr "Caractere realistice moderate în situații nesigure"

#~ msgid "Depictions of realistic characters in aggressive conflict"
#~ msgstr "Descrieri ale unor caractere realistice în conflict agresiv"

#~ msgid "Graphic violence involving realistic characters"
#~ msgstr "Violență grafică incluzând caractere realistice"

#~ msgid "No bloodshed"
#~ msgstr "Fără vărsare de sânge"

#~ msgid "Unrealistic bloodshed"
#~ msgstr "Vărsare de sânge nerealistică"

#~ msgid "Realistic bloodshed"
#~ msgstr "Vărsare de sânge realistică"

#~ msgid "Depictions of bloodshed and the mutilation of body parts"
#~ msgstr "Descrieri de vărsare de sânge și mutilarea părților de corp"

#~ msgid "No sexual violence"
#~ msgstr "Fără violență sexuală"

#~ msgid "Rape or other violent sexual behavior"
#~ msgstr "Viol sau alt comportament sexual"

#~ msgid "No references to alcohol"
#~ msgstr "Fără referințe la băuturi alcoolice"

#~ msgid "References to alcoholic beverages"
#~ msgstr "Referințe la băuturi alcoolice"

#~ msgid "Use of alcoholic beverages"
#~ msgstr "Uz de băuturi alcoolice"

#~ msgid "No references to illicit drugs"
#~ msgstr "Fără referințe la droguri ilegale"

#~ msgid "References to illicit drugs"
#~ msgstr "Referințe la droguri ilegale"

#~ msgid "Use of illicit drugs"
#~ msgstr "Uz de droguri ilegale"

#~ msgid "References to tobacco products"
#~ msgstr "Referințe la produse de tutun"

#~ msgid "Use of tobacco products"
#~ msgstr "Uz de produse de tutun"

#~ msgid "No nudity of any sort"
#~ msgstr "Fără nuditate de orice tip"

#~ msgid "Brief artistic nudity"
#~ msgstr "Scurtă nuditate artistică"

#~ msgid "Prolonged nudity"
#~ msgstr "Nuditate prelungită"

#~ msgid "No references or depictions of sexual nature"
#~ msgstr "Fără referințe sau descrieri de natură sexuală"

#~ msgid "Provocative references or depictions"
#~ msgstr "Referințe sau descrieri provocative"

#~ msgid "Sexual references or depictions"
#~ msgstr "Referințe sexuale sau descrieri"

#~ msgid "Graphic sexual behavior"
#~ msgstr "Comportament sexual grafic"

#~ msgid "No profanity of any kind"
#~ msgstr "Fără profanitate de orice tip"

#~ msgid "Mild or infrequent use of profanity"
#~ msgstr "Uz moderat sau nefrecvent de obscenitate"

#~ msgid "Moderate use of profanity"
#~ msgstr "Uz moderat de obscenitate"

#~ msgid "Strong or frequent use of profanity"
#~ msgstr "Uz puternic sau frecvent de obscenitate"

#~ msgid "No inappropriate humor"
#~ msgstr "Fără umor inapropriat"

#~ msgid "Slapstick humor"
#~ msgstr "Umor ieftin"

#~ msgid "Vulgar or bathroom humor"
#~ msgstr "Umor vulgar"

#~ msgid "Mature or sexual humor"
#~ msgstr "Umor pentru adulți sau sexual"

#~ msgid "No discriminatory language of any kind"
#~ msgstr "Fără limbaj discriminator de orice tip"

#~ msgid "Negativity towards a specific group of people"
#~ msgstr "Negativitate referitor la un grup specific de oameni"

#~ msgid "Discrimination designed to cause emotional harm"
#~ msgstr "Discriminare destinată să cauzeze ofensă emoțională"

#~ msgid "Explicit discrimination based on gender, sexuality, race or religion"
#~ msgstr "Discriminare explicită bazată pe sex, sexualitate, rasă sau religie"

#~ msgid "No advertising of any kind"
#~ msgstr "Fără reclame de orice tip"

#~ msgid "Product placement"
#~ msgstr "Plasarea de produse"

#~ msgid "Explicit references to specific brands or trademarked products"
#~ msgstr ""
#~ "Referințe explicite la mărci specifice sau produse de mărci înregistrate"

#~ msgid "Users are encouraged to purchase specific real-world items"
#~ msgstr ""
#~ "Utilizatorii sunt încurajați să cumpere obiecte specifice din lumea reală"

#~ msgid "No gambling of any kind"
#~ msgstr "Fără jocuri de noroc de orice tip"

#~ msgid "Gambling on random events using tokens or credits"
#~ msgstr ""
#~ "Jocuri de noroc la evenimente aleatorii folosind jetoane sau credite"

#~ msgid "Gambling using “play” money"
#~ msgstr "Jocuri de noroc folosind bani virtuali"

#~ msgid "Gambling using real money"
#~ msgstr "Jocuri de noroc folosind bani adevărați"

#~ msgid "No ability to spend money"
#~ msgstr "Fără abilitatea de a cheltui bani"

#~ msgid "Users are encouraged to donate real money"
#~ msgstr "Utilizatorii sunt încurajați să doneze bani adevărați"

#~ msgid "Ability to spend real money in-game"
#~ msgstr "Abilitatea de a cheltui bani reali în joc"

#~ msgid "No way to chat with other users"
#~ msgstr "Fără abilitatea de a discuta cu alți utilizatori"

#~ msgid "User-to-user game interactions without chat functionality"
#~ msgstr "Interacțiuni utilizator-la-utilizator fără funcționalitate de chat"

#~ msgid "Moderated chat functionality between users"
#~ msgstr "Funcționalitate de chat moderat între utilizatori"

#~ msgid "Uncontrolled chat functionality between users"
#~ msgstr "Funcționalitate de chat necontrolat între utilizatori"

#~ msgid "No way to talk with other users"
#~ msgstr "Fără abilitatea de a vorbi cu alți utilizatori"

#~ msgid "Uncontrolled audio or video chat functionality between users"
#~ msgstr ""
#~ "Funcționalitate de chat necontrolat audio sau video între utilizatori"

#~ msgid "No sharing of social network usernames or email addresses"
#~ msgstr ""
#~ "Fără partajare de nume de utilizatori de rețele sociale sau adrese de "
#~ "email"

#~ msgid "Sharing social network usernames or email addresses"
#~ msgstr ""
#~ "Partajare de nume de utilizatori de rețele sociale sau adrese de email"

#~ msgid "No sharing of user information with 3rd parties"
#~ msgstr "Fără partajare de informații despre utilizatori cu părți terțe"

#~ msgid "Checking for the latest application version"
#~ msgstr "Verificare după ultima versiune de aplicație"

#~ msgid "Sharing diagnostic data that does not let others identify the user"
#~ msgstr ""
#~ "Partajare de date diagnostice care nu permit altora identificarea "
#~ "utilizatorului"

#~ msgid "Sharing information that lets others identify the user"
#~ msgstr ""
#~ "Partajare de informații care permit altora identificarea utilizatorului"

#~ msgid "No sharing of physical location to other users"
#~ msgstr "Fără partajarea locației fizice cu alți utilizatori"

#~ msgid "Sharing physical location to other users"
#~ msgstr "Partajarea locației fizice cu alți utilizatori"

#~ msgid "No references to homosexuality"
#~ msgstr "Fără referințe la homosexualitate"

#~ msgid "Indirect references to homosexuality"
#~ msgstr "Referințe indirecte la homosexualitate"

#~ msgid "Kissing between people of the same gender"
#~ msgstr "Sărutarea între oameni de același sex"

#~ msgid "Graphic sexual behavior between people of the same gender"
#~ msgstr "Comportament sexual grafic între oameni de același sex"

#~ msgid "No references to prostitution"
#~ msgstr "Fără referințe la prostituție"

#~ msgid "Indirect references to prostitution"
#~ msgstr "Referințe indirecte la prostituție"

#~ msgid "Direct references to prostitution"
#~ msgstr "Referințe directe la prostituție"

#~ msgid "Graphic depictions of the act of prostitution"
#~ msgstr "Descrieri grafice ale actului de prostituție"

#~ msgid "No references to adultery"
#~ msgstr "Fără referințe la adulter"

#~ msgid "Indirect references to adultery"
#~ msgstr "Referințe indirecte la adulter"

#~ msgid "Direct references to adultery"
#~ msgstr "Referințe directe de adulter"

#~ msgid "Graphic depictions of the act of adultery"
#~ msgstr "Descrieri grafice ale actului de adulter"

#~ msgid "No sexualized characters"
#~ msgstr "Fără caractere sexuale"

#~ msgid "Scantily clad human characters"
#~ msgstr "Caractere umane îmbrăcate sumar"

#~ msgid "Overtly sexualized human characters"
#~ msgstr "Caractere umane sexualizate deschis"

#~ msgid "No references to desecration"
#~ msgstr "Fără referințe la profanare"

#~ msgid "Depictions or references to historical desecration"
#~ msgstr "Depictări sau referințe la profanare istorică"

#~ msgid "Depictions of modern-day human desecration"
#~ msgstr "Descrieri de profanare umană modernă"

#~ msgid "Graphic depictions of modern-day desecration"
#~ msgstr "Descrieri grafice de profanare modernă"

#~ msgid "No visible dead human remains"
#~ msgstr "Fără rămășițe umane vizibile"

#~ msgid "Visible dead human remains"
#~ msgstr "Rămășițe umane vizibile"

#~ msgid "Dead human remains that are exposed to the elements"
#~ msgstr "Rămășițe umane are expuse elementelor"

#~ msgid "Graphic depictions of desecration of human bodies"
#~ msgstr "Descrieri grafice ale profanării corpurilor umane"

#~ msgid "No references to slavery"
#~ msgstr "Fără referințe la sclavagism"

#~ msgid "Depictions or references to historical slavery"
#~ msgstr "Depictări sau referințe la sclavagismul istoric"

#~ msgid "Depictions of modern-day slavery"
#~ msgstr "Descrieri ale sclavagismului modern"

#~ msgid "Graphic depictions of modern-day slavery"
#~ msgstr "Descrieri grafice ale sclavagismului modern"
